package main

import (
	"os"

	"gitlab.com/gelora-app/notification-service/cmd"
)

func main() {
	app := cmd.RunServer()
	if err := app.Execute(); err != nil {
		os.Exit(1)
	}
}
