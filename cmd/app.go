package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/gelora-app/notification-service/pkg/container"
)

func RunServer() *cobra.Command {
	appCmd := &cobra.Command{
		Use:   "notification-service",
		Short: "Start Notification Service",
		Long:  "Start Notification Service",
		Run:   NotificationServer,
	}

	return appCmd
}

func NotificationServer(cmd *cobra.Command, args []string) {
	container.NewContainer()
}
