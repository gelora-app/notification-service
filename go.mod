module gitlab.com/gelora-app/notification-service

go 1.16

require (
	github.com/Shopify/sarama v1.29.1
	github.com/klauspost/compress v1.13.1 // indirect
	github.com/spf13/cobra v1.2.1
)
