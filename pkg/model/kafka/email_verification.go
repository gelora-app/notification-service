package model_kafka

type EmailVerification struct {
	FullName string `json:"fullName"`
	Email    string `json:"email"`
	Token    string `json:"token"`
}
