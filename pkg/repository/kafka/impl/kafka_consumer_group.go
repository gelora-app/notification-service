package kafka_impl

import (
	"github.com/Shopify/sarama"
	"gitlab.com/gelora-app/notification-service/pkg/constants"
	service "gitlab.com/gelora-app/notification-service/pkg/service/impl"
)

type KafkaConsumerGroupRepository struct {
	Ready chan bool
}

func (consumerGroup *KafkaConsumerGroupRepository) Setup(sarama.ConsumerGroupSession) error {
	close(consumerGroup.Ready)
	return nil
}

func (consumerGroup *KafkaConsumerGroupRepository) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

func (consumerGroup *KafkaConsumerGroupRepository) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for message := range claim.Messages() {

		if message.Topic == constants.TOPIC_SEND_EMAIL_VERIFICATION {
			service.EmailVerification(message.Value)
		}

		session.MarkMessage(message, "CONSUMED")
	}

	return nil
}
