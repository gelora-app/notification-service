package container

import (
	"context"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/Shopify/sarama"
	"gitlab.com/gelora-app/notification-service/pkg/constants"
	kafkaRepo "gitlab.com/gelora-app/notification-service/pkg/repository/kafka/impl"
)

func NewContainer() {
	config := sarama.NewConfig()

	consumer := kafkaRepo.KafkaConsumerGroupRepository{Ready: make(chan bool)}

	client, err := sarama.NewConsumerGroup([]string{constants.KAFKA_ADDRESS}, constants.GROUP_NOTIFICATION_SERVICE, config)
	if err != nil {
		log.Panicf("Error creating consumer group client: %v", err)
	}

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			if err := client.Consume(context.Background(), []string{constants.TOPIC_SEND_EMAIL_VERIFICATION}, &consumer); err != nil {
				log.Panicf("Error from consumer: %v", err)
			}

			consumer.Ready = make(chan bool)
		}
	}()

	<-consumer.Ready
	log.Println("Consumer up and running...")
	wg.Wait()

	sigterm := make(chan os.Signal, 1)
	done := make(chan bool, 1)
	signal.Notify(sigterm, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigterm
		done <- true
	}()
	<-done
}
