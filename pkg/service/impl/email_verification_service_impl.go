package impl

import (
	"encoding/json"
	"log"

	modelKafka "gitlab.com/gelora-app/notification-service/pkg/model/kafka"
)

func EmailVerification(request []byte) {
	var requestStruct modelKafka.EmailVerification
	err := json.Unmarshal(request, &requestStruct)
	if err != nil {
		log.Panicf("Error parse json %v", err)
	}
}
